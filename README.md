# PROYECTO API Backend Dream Banck

## How to use this template 

- Create your virtualenv
- `git clone https://gitlab.com/sb1190/backenddreambank`
- Rename the folder to your project name.
- `cd PROJECTNAME`
- `pip install -r requirements/dev.txt`
- `python run.py`

ORM:

- [SQLAlchemy](https://www.sqlalchemy.org/)
- [flask-sqlalchemy](https://flask-sqlalchemy.palletsprojects.com/en/2.x/)

DOCKER:
- `docker build -t "backend-dream-bank" .\`
- `docker run -d -p 5000:5000  --name="backend-dream-bank" "backend-dream-bank"`
