FROM tiangolo/uwsgi-nginx-flask:python3.8
WORKDIR /app
COPY ./requirements.txt /var/www/requirements.txt
RUN pip install -r /var/www/requirements.txt
COPY . .