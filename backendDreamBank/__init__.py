import os

from flask import Flask

from backendDreamBank.extensions.extensions import *

#############################################
# App Configuration
#############################################

app = Flask(__name__)
app.config.from_object(
    os.environ.get(
        'APP_SETTINGS',
        'backendDreamBank.config.dev_config.DevelopmentConfig'
    )
)

db.init_app(app)
jwt.init_app(app)
cors.init_app(app, resources={r"/api/v1/*": {"origins": "*"}})

#############################################
# Blueprints
#############################################

from backendDreamBank.modules.auth.resources import auth_v1
from backendDreamBank.modules.transactions.resources import transaction_v1

app.register_blueprint(auth_v1, url_prefix='/api/v1')
app.register_blueprint(transaction_v1, url_prefix='/api/v1')

#############################################
# Injector
#############################################
from flask_injector import FlaskInjector
from backendDreamBank.modules.utilities.dependencies.dependencies import utils_configure
from backendDreamBank.modules.auth.dependencies.dependencies import auth_configure

FlaskInjector(app=app, modules=[utils_configure, auth_configure])

#############################################
# Errors
#############################################
from backendDreamBank.exceptions.register_errors import register_error_handlers

register_error_handlers(app)

#############################################
# Create user
#############################################
from backendDreamBank.modules.auth.entities.user_entity import UserEntity
from backendDreamBank.modules.auth.repositories.auth_repository import AuthRepository


@app.before_first_request
def create_tables():
    db.create_all()
    if db.session.query(UserEntity).count() == 0:
        user = UserEntity()
        user.dni = "1128433566"
        user.full_name = 'Victor Warren'
        user.password = AuthRepository().created_password("123456")

        AuthRepository().add(user)


if __name__ == '__main__':
    app.run(app.config["PORT"])
