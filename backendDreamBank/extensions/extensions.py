# flask SQLAlchemy
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()

# Flask restful api
from flask_restful import Api
api = Api()

# Flask jwt extended
from flask_jwt_extended import JWTManager
jwt = JWTManager()


# Flask-Cors
from flask_cors import CORS
cors = CORS()
