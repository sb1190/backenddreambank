from flask_jwt_extended import create_access_token, create_refresh_token, get_jwt_identity

from backendDreamBank.modules.auth.repositories.auth_repository import *
from backendDreamBank.modules.auth.services.auth_utilities_service import *
from backendDreamBank.const.response_const import *
from backendDreamBank.modules.utilities.response.reponse_service import ResponseService


class AuthService:

    @inject
    def __init__(self,
                 response_service: ResponseService,
                 auth_repository: AuthRepository,
                 auth_utilities_service: AuthUtilitiesService):
        self._auth_repository = auth_repository
        self._auth_utilities_service = auth_utilities_service
        self._response_service = response_service

    def login(self, dni: str, password: str) -> ResponseService.response:
        user = self._auth_utilities_service.user_dni_exits(dni=dni)
        if self._auth_repository.verify_password(user=user, password=password):
            return self._response_service.response(
                status_code=200,
                data={
                    "accessToken": create_access_token(identity=user.id, fresh=True),
                    "refreshToken": create_refresh_token(user.id),
                    "user": user.user_presenter()
                })
        else:
            return self._response_service.abort(message=auth_invalid, status_code=401)

    def refresh_token(self) -> ResponseService.response:
        current_user = get_jwt_identity()
        new_token = create_access_token(identity=current_user, fresh=False)
        return self._response_service.response(
            success=True,
            status_code=200,
            data={"accessToken": new_token}
        )
