from injector import inject

from backendDreamBank.modules.auth.entities.user_entity import UserEntity
from backendDreamBank.modules.utilities.response.reponse_service import ResponseService
from backendDreamBank.modules.auth.repositories.user_repository import UserRepository
from backendDreamBank.const.response_const import auth_invalid


class AuthUtilitiesService:

    @inject
    def __init__(self,
                 user_repository: UserRepository,
                 response_service: ResponseService):
        self._user_repository = user_repository
        self._response_service = response_service

    def user_dni_exits(self, dni: str) -> UserEntity:
        user = self._user_repository.get_by_dni(dni)
        if user:
            return user
        else:
            return self._response_service.abort(message=auth_invalid, status_code=401)
