from flask_jwt_extended import jwt_refresh_token_required
from flask_restful_swagger import swagger
from flask_restful import Resource
from injector import inject

from backendDreamBank.modules.auth.services.auth_service import AuthService


class AuthTokenRefreshResource(Resource):

    @inject
    def __init__(self, auth_service: AuthService):
        self._auth_service = auth_service

    @swagger.operation(
        notes="",
        summary="",
        type="",
        parameters=[
            {
                "name": "Authorization",
                "description": "Must be the refresh token eyJ0eXAiOiJK ...",
                "required": True,
                "allowMultiple": False,
                "dataType": "str",
                "paramType": "header"
            }
        ]
    )
    @jwt_refresh_token_required
    def post(self):
        return self._auth_service.refresh_token()
