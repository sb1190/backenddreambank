from flask_restful_swagger import swagger
from flask_restful import Resource, reqparse
from injector import inject

from backendDreamBank.const.validators_const import not_blank
from backendDreamBank.modules.auth.services.auth_service import AuthService


class AuthLoginResource(Resource):

    @inject
    def __init__(self, auth_service: AuthService):
        self._auth_service = auth_service

    @swagger.operation(
        notes="",
        summary="",
        type="",
        parameters=[
            {
                "name": "body",
                "description": '{ "dni": "112856677","password": "123456" }',
                "required": True,
                "allowMultiple": False,
                "dataType": "application/json",
                "paramType": "body"
            }
        ]
    )
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("dni", type=str, required=True, help=not_blank)
        parser.add_argument("password", type=str, required=True, help=not_blank)
        kwargs = parser.parse_args(strict=True)
        return self._auth_service.login(**kwargs)
