from flask import Blueprint
from flask_restful import Api
from flask_restful_swagger import swagger

from backendDreamBank import app

from backendDreamBank.modules.auth.resources.auth_login_resource import AuthLoginResource
from backendDreamBank.modules.auth.resources.auth_token_refresh_resource import AuthTokenRefreshResource

auth_v1 = Blueprint('auth_v1', __name__, template_folder='../templates/')

# Set up the API and init the blueprint
api = swagger.docs(Api(auth_v1), apiVersion='1.0',
                   basePath=app.config['HOST'],
                   resourcePath='/',
                   produces=["application/json", "text/html"],
                   api_spec_url='/doc/auth',
                   description='Api backendDreamBank Version 1.0')

#############################################
# Resources to Add
#############################################

# auth
api.add_resource(AuthLoginResource, '/auth/login')
api.add_resource(AuthTokenRefreshResource, '/auth/refresh')
