from flask import request

from backendDreamBank.modules.auth.services.auth_service import AuthService
from backendDreamBank.modules.auth.services.auth_utilities_service import AuthUtilitiesService
from backendDreamBank.modules.auth.repositories.auth_repository import AuthRepository
from backendDreamBank.modules.auth.repositories.user_repository import UserRepository


def auth_configure(binder):
    binder.bind(AuthService, to=AuthService, scope=request)
    binder.bind(AuthUtilitiesService, to=AuthUtilitiesService, scope=request)
    binder.bind(AuthRepository, to=AuthRepository, scope=request)
    binder.bind(UserRepository, to=UserRepository, scope=request)
