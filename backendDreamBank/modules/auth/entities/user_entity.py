from datetime import datetime

from flask_restful_swagger import swagger
from flask_restful import fields

from backendDreamBank.extensions.extensions import db


@swagger.model
class UserEntity(db.Model):
    __tablename__ = "tbl_users"

    id = db.Column(db.Integer, primary_key=True)
    full_name = db.Column(db.String(255), unique=False, nullable=True)
    dni = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    delete_on = db.Column(db.DateTime, nullable=True)
    created_on = db.Column(db.DateTime, default=datetime.now)
    updated_on = db.Column(db.DateTime, nullable=False, default=datetime.now, onupdate=datetime.now)
    is_active = db.Column(db.Boolean(), default=True)

    def __str__(self):
        return self.dni

    # Definition of the model that appears in the documentation of the api.
    resource_fields = {
        "id": fields.Integer,
        "dni": fields.String,
        "password": fields.String,
        "created_on": fields.DateTime,
        "updated_on": fields.DateTime,
    }

    # Mark required fields in the documentation model.
    required = ["dni", "password"]

    def user_presenter(self) -> {}:
        return {
            'id': self.id,
            'dni': self.dni,
            'fullName': self.full_name
        }
