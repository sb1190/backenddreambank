from werkzeug.security import generate_password_hash, check_password_hash

from backendDreamBank.modules.auth.entities.user_entity import UserEntity
from backendDreamBank.modules.utilities.repository.base_repository import BaseRepository


class AuthRepository(BaseRepository):

    def __init__(self):
        super().__init__(entity=UserEntity)

    def verify_password(self, user, password: str) -> bool:
        return self.__verify_password(password_user=user.password, password=password)

    def created_password(self, password: str) -> str:
        return self.__created_password(password=password)

    @staticmethod
    def __created_password(password: str) -> str:
        try:
            return generate_password_hash(password)
        except Exception as error:
            raise error

    @staticmethod
    def __verify_password(password_user: str, password: str) -> bool:
        try:
            return check_password_hash(password_user, password)
        except Exception as error:
            raise error
