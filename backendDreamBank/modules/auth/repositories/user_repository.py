from backendDreamBank.modules.utilities.repository.base_repository import BaseRepository
from backendDreamBank.modules.auth.entities.user_entity import UserEntity


class UserRepository(BaseRepository):
    __entity = UserEntity

    def __init__(self):
        super().__init__(entity=UserEntity)

    def get_by_dni(self, dni: str) -> UserEntity:
        try:
            return self.__entity.query.filter_by(dni=dni.lower(), is_active=True).first()
        except Exception as error:
            raise error
