from backendDreamBank.modules.transactions.entities.account_detail_entity import AccountDetailEntity


class AccountDetailRepository:

    @staticmethod
    def accounts_detail_by_id(id: int) -> [AccountDetailEntity]:
        return [
            {
                'id': 1,
                'date': '10/12/2020',
                'description': 'PAYMENT VIRT AS',
                'currency': 'USD',
                'value': '6266.33',
                'balance': '6266.33',
            },
            {
                'id': 2,
                'date': '11/12/2020',
                'description': 'INT IVA',
                'currency': 'USD',
                'value': '10998.10',
                'balance': '10998.10',
            },
            {
                'id': 3,
                'date': '12/12/2020',
                'description': 'PAYMENT VIRT AS',
                'currency': 'USD',
                'value': '23.86',
                'balance': '-23.86',
            },
            {
                'id': 4,
                'date': '14/12/2020',
                'description': 'INT IVA',
                'currency': 'USD',
                'value': '23.86',
                'balance': '-23.86',
            }
        ]
