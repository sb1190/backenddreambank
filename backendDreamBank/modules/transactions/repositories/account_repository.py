from backendDreamBank.modules.transactions.entities.account_entity import AccountEntity


class AccountRepository:

    @staticmethod
    def accounts_by_id(id: int) -> [AccountEntity]:
        return [
            {
                'id': 1,
                'type': 'Checking',
                'name': '126745**** - WOLFE',
                'status': True,
                'currency': 'USD',
                'balance': '6266.33',
            },
            {
                'id': 2,
                'type': 'Savings',
                'name': '5719371**** - MAENGUNE',
                'status': True,
                'currency': 'USD',
                'balance': '10998.10',
            },
            {
                'id': 3,
                'type': 'Savings',
                'name': '7125781**** - KAISER',
                'status': False,
                'currency': 'USD',
                'balance': '23.86',
            }
        ]
