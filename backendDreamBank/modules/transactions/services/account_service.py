from injector import inject

from backendDreamBank.modules.utilities.response.reponse_service import ResponseService
from backendDreamBank.modules.transactions.repositories.account_repository import AccountRepository


class AccountService:

    @inject
    def __init__(self,
                 response_service: ResponseService,
                 account_repository: AccountRepository):
        self._account_repository = account_repository
        self._response_service = response_service

    def user_accounts(self, id: int) -> ResponseService.response:
        return self._response_service.response(
            status_code=200,
            data={
                "accounts": self._account_repository.accounts_by_id(id)
            })
