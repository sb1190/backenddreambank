from injector import inject

from backendDreamBank.modules.utilities.response.reponse_service import ResponseService
from backendDreamBank.modules.transactions.repositories.request_product_repository import RequestProductRepository
from backendDreamBank.modules.transactions.entities.request_product_entity import RequestProductEntity


class RequestProductService:

    @inject
    def __init__(self,
                 response_service: ResponseService,
                 request_product_repository: RequestProductRepository):
        self._request_product_repository = request_product_repository
        self._response_service = response_service

    def add_request_product(self,
                            request_product: RequestProductEntity) -> ResponseService.response:
        self._request_product_repository.add_request_product(request_product=request_product)
        return self._response_service.response(
            status_code=201,
            data={
                'id': 1,
                'Cellphone': request_product.cellphone,
                'monthly_income': request_product.monthly_income
            })
