from injector import inject

from backendDreamBank.modules.utilities.response.reponse_service import ResponseService
from backendDreamBank.modules.transactions.repositories.account_detail_repository import AccountDetailRepository


class AccountDetailService:

    @inject
    def __init__(self,
                 response_service: ResponseService,
                 account_detail_repository: AccountDetailRepository):
        self._account_detail_repository = account_detail_repository
        self._response_service = response_service

    def account_detail(self, id: int) -> ResponseService.response:
        return self._response_service.response(
            status_code=200,
            data={
                "accountDetail": self._account_detail_repository.accounts_detail_by_id(id)
            })
