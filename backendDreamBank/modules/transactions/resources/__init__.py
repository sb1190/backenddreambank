from flask import Blueprint
from flask_restful import Api
from flask_restful_swagger import swagger

from backendDreamBank import app

from backendDreamBank.modules.transactions.resources.account_resource import AccountResource
from backendDreamBank.modules.transactions.resources.account_detail_resource import AccountDetailResource
from backendDreamBank.modules.transactions.resources.request_product_resource import RequestProductResource

transaction_v1 = Blueprint('transaction_v1', __name__, template_folder='../templates/')

# Set up the API and init the blueprint
api = swagger.docs(Api(transaction_v1), apiVersion='1.0',
                   basePath=app.config['HOST'],
                   resourcePath='/',
                   produces=["application/json", "text/html"],
                   api_spec_url='/doc/transaction',
                   description='Api backendDreamBank Version 1.0')

#############################################
# Resources to Add
#############################################

# accounts
api.add_resource(AccountResource, '/account/<int:id>')
api.add_resource(AccountDetailResource, '/account-detail/<int:id>')
api.add_resource(RequestProductResource, '/request-product')
