from flask_jwt_extended import jwt_required
from flask_restful_swagger import swagger
from flask_restful import Resource
from injector import inject

from backendDreamBank.modules.transactions.services.account_service import AccountService


class AccountResource(Resource):

    @inject
    def __init__(self, account_service: AccountService):
        self._account_service = account_service

    @swagger.operation(
        notes="",
        summary="",
        type="",
        parameters=[
            {
                "name": "Authorization",
                "description": "eyJ0eXAiOiJK ...",
                "required": True,
                "allowMultiple": False,
                "dataType": "str",
                "paramType": "header"
            }
        ]
    )
    @jwt_required
    def get(self, id: int):
        return self._account_service.user_accounts(id=id)
