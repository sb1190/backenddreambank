from flask_jwt_extended import jwt_required
from flask_restful_swagger import swagger
from flask_restful import Resource, reqparse
from injector import inject

from backendDreamBank.modules.transactions.services.request_product_service import RequestProductService
from backendDreamBank.modules.transactions.entities.request_product_entity import RequestProductEntity


class RequestProductResource(Resource):

    @inject
    def __init__(self, request_product_service: RequestProductService):
        self._request_product_service = request_product_service

    @swagger.operation(
        notes="",
        summary="",
        type="",
        parameters=[
            {
                "name": "Authorization",
                "description": "eyJ0eXAiOiJK ...",
                "required": True,
                "allowMultiple": False,
                "dataType": "str",
                "paramType": "header"
            },
            {
                "name": "",
                "description": '{"id": 1,"cellphone": "123456789", "monthly": 10000  }',
                "required": True,
                "allowMultiple": False,
                "dataType": "application/json",
                "paramType": "body"
            }
        ]
    )
    @jwt_required
    def post(self):
        parser = reqparse.RequestParser()
        parser.add_argument("id", type=int, required=True)
        parser.add_argument("cellphone", type=str, required=True)
        parser.add_argument("monthly", type=int, required=True)
        kwargs = parser.parse_args(strict=True)

        request_product = RequestProductEntity()
        request_product.id = kwargs.id
        request_product.cellphone = kwargs.cellphone
        request_product.monthly_income = kwargs.monthly

        return self._request_product_service.add_request_product(request_product=request_product)
