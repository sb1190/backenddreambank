from flask_restful_swagger import swagger
from flask_restful import fields


@swagger.model
class AccountEntity:
    id: int
    type: str
    name: str
    status: bool
    currency: str
    balance: float

    def __str__(self):
        return self.name

    # Definition of the model that appears in the documentation of the api.
    resource_fields = {
        "id": fields.Integer,
        "type": fields.String,
        "name": fields.String,
        "status": fields.Boolean,
        "currency": fields.String,
        "balance": fields.Float,
    }
