from flask_restful_swagger import swagger
from flask_restful import fields


@swagger.model
class RequestProductEntity:
    id: int
    cellphone: str
    monthly_income: float

    def __str__(self):
        return self.Product

    # Definition of the model that appears in the documentation of the api.
    resource_fields = {
        "id": fields.Integer,
        "Product": fields.String,
        "Cellphone": fields.String,
        "monthly_income": fields.Integer,
    }
