from flask_restful_swagger import swagger
from flask_restful import fields


@swagger.model
class AccountDetailEntity:
    id: int
    date: str
    description: str
    currency: str
    value: float
    balance: float

    def __str__(self):
        return self.date

    # Definition of the model that appears in the documentation of the api.
    resource_fields = {
        "id": fields.Integer,
        "date": fields.String,
        "description": fields.String,
        "currency": fields.String,
        "value": fields.Float,
        "balance": fields.Float,
    }
