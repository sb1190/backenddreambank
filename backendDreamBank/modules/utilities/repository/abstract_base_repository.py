import abc


class AbstractBaseRepository(abc.ABC):

    @abc.abstractmethod
    def get_by_id(self, id):
        raise NotImplementedError
