from backendDreamBank.modules.utilities.repository.abstract_base_repository import AbstractBaseRepository


class BaseRepository(AbstractBaseRepository):

    def __init__(self, entity):
        self.entity = entity

    def get_by_id(self, id: int):
        try:
            return self.entity.query.filter_by(id=id).first()
        except Exception as error:
            raise error
