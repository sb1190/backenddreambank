from flask import abort, Response
from json import dumps


class ResponseService:

    @staticmethod
    def response(status_code: int, success: bool = True, message: str = None, data: any = None):
        return {"success": success, "message": message, "data": data, }, status_code

    @staticmethod
    def abort(status_code: int, success: bool = False, message: str = None, data: any = None):
        return abort(
            Response(
                dumps({"success": success, "message": message, "data": data}), status_code
            )
        )
