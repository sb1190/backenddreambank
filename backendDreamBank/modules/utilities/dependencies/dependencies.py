from flask import request

from backendDreamBank.modules.utilities.response.reponse_service import ResponseService


def utils_configure(binder):
    binder.bind(ResponseService, to=ResponseService, scope=request)

