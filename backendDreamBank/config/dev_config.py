from backendDreamBank.config.base_config import BaseConfig


class DevelopmentConfig(BaseConfig):
    ENVIRONMENT = "development"
    DEBUG = True

    # Host
    HOST = "http://localhost:5000"

    #
    SECRET_KEY = "super secret key"
    SECURITY_PASSWORD_SALT = "my_secret"

    # DB options
    SQLALCHEMY_DATABASE_URI = "postgresql://postgres:postgres@localhost:5432/dream_bank"
    PROPAGATE_EXCEPTIONS = True
