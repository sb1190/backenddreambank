class BaseConfig(object):
    DEBUG = False
    PORT = 5000

    # DB
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    PROPAGATE_EXCEPTIONS = False

    # Flask jwt extended
    JWT_HEADER_TYPE = ""
    # enable blacklist feature
    JWT_BLACKLIST_ENABLED = False
    # allow blacklisting for access and refresh tokens
    JWT_BLACKLIST_TOKEN_CHECKS = ["access", "refresh"]
