import os

from backendDreamBank.config.base_config import BaseConfig


class ProductionConfig(BaseConfig):
    ENVIRONMENT = "production"
    # Host
    HOST = ""

    #
    SECRET_KEY = os.environ['SECRET_KEY']
    SECURITY_PASSWORD_SALT = os.environ['SECURITY_PASSWORD_SALT']

    # DB options
    SQLALCHEMY_DATABASE_URI = os.environ['SQLALCHEMY_DATABASE_URI']

