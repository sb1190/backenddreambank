from flask import jsonify

from backendDreamBank.const.response_const import error_500


def register_error_handlers(app):
    @app.errorhandler(Exception)
    def handle_exception_error(e):
        return jsonify({"success": False, "message": error_500, "data": None}), 500
