from setuptools import setup

setup(
    name='backendDreamBank',
    packages=[],
    include_package_data=True,
    install_requires=[
        'flask',
    ],
)
